<?php global $product; ?>
<li>
	<?php echo '<a href="'.get_permalink( $product->id ).'" title="'.$product->get_title().'">'; ?>
		<?php echo $product->get_image(); ?>
		<span class="product-title"><?php echo $product->get_title(); ?></span>
	</a>
	<?php if ( ! empty( $show_rating ) ) echo $product->get_rating_html(); ?>
	<?php echo $product->get_price_html(); ?>
</li>